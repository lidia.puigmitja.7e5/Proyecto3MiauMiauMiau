package com.example.proyecto3gatitosmiaumiaumiau.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Image (
    @SerialName("id"     ) var id     : String? = null,
    @SerialName("width"  ) var width  : Int?    = null,
    @SerialName("height" ) var height : Int?    = null,
    @SerialName("url"    ) var url    : String? = null
        )