package com.example.proyecto3gatitosmiaumiaumiau

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.proyecto3gatitosmiaumiaumiau.ui.GatitosPhotosApp
import com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Proyecto3GatitosMiauMiauMiauTheme

class GatitosActivityInicial : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Proyecto3GatitosMiauMiauMiauTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                  //  color = MaterialTheme.colors.background
                ) {
                    GatitosPhotosApp()

                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview3() {
   Proyecto3GatitosMiauMiauMiauTheme {
       // Greeting("Android")
    }
}