import com.example.proyecto3gatitosmiaumiaumiau.model.GatitosPhoto
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Headers


private const val BASE_URL=
    "https://api.thecatapi.com/v1/"

private const val token= "live_rUXLiMjN1x2rBw0b1fRd7diIIfVQlouJ6HHJHCzIfu9ekVN7pTqm8JoTmB4n78lH"

private val jsonIgnored= Json{ignoreUnknownKeys=true}

private val retrofit= Retrofit.Builder()
    .addConverterFactory(jsonIgnored.asConverterFactory("application/json".toMediaType()))
    .baseUrl(BASE_URL)
    .build()

interface GatitosApiService{
    @Headers("x-api-key: $token")
    @GET("breeds")
    suspend fun getPhotos(): List<GatitosPhoto>
}

object GatitosApi{
    val retrofitService: GatitosApiService by lazy{
        retrofit.create(GatitosApiService::class.java)
    }
}