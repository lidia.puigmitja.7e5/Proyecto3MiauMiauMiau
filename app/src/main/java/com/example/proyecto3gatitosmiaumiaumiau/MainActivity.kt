package com.example.proyecto3gatitosmiaumiaumiau

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.OptIn
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.*
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.proyecto3gatitosmiaumiaumiau.ui.GatitosPhotosApp
import com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Proyecto3GatitosMiauMiauMiauTheme
import com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Shapes
import com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Typography
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.sql.DriverManager.println

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Proyecto3GatitosMiauMiauMiauTheme {
               // GatitosPhotosApp()
                // A surface container using the 'background' color from the theme
               /* Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {*/

                    ShowLogIn(modifier = Modifier.fillMaxSize().background(color = MaterialTheme.colors.background))
//                }
            }
        }
    }
}



@Composable
fun ShowLogIn(modifier: Modifier) {
  //  var textMail by remember { mutableStateOf(TextFieldValue("")) }
//    var userGuess by mutableStateOf("")
//    private set

    Column(modifier, verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally){

        GlideImage(
            imageModel = { (R.drawable.gatitobonito) }, modifier = Modifier
                .height(300.dp),
            imageOptions = ImageOptions(contentScale = ContentScale.Crop)
        )
        Spacer(modifier = Modifier.height(30.dp))
        TextFieldWithIconEmail()
        Spacer(modifier = Modifier.height(30.dp))
        TextFieldWithPassword()
        Spacer(modifier = Modifier.height(35.dp))
        ButtonOne()
      /*  Text(
            text = ("Hello $name!"),
            style = Typography.body1,
            color = MaterialTheme.colors.primary,
            modifier = Modifier
                .padding(8.dp)
        )*/
    }
}

@Composable
fun TextFieldWithIconEmail() {
    var textMail by remember { mutableStateOf(TextFieldValue("")) }
     OutlinedTextField(
        value = textMail,
        leadingIcon = { Icon(imageVector = Icons.Default.Email, contentDescription = "emailIcon", tint = MaterialTheme.colors.secondary,) },
        keyboardOptions = KeyboardOptions.Default.copy( imeAction = ImeAction.Next, keyboardType = KeyboardType.Email),
        onValueChange = {
            textMail = it
        },
        label = { Text(text = "Email address",style = Typography.body2,color = MaterialTheme.colors.secondary) },
        placeholder = { Text(text = "Enter your e-mail",style = Typography.body1,color = MaterialTheme.colors.primary) },
         colors = TextFieldDefaults.outlinedTextFieldColors(
             focusedBorderColor = MaterialTheme.colors.secondary,
             unfocusedBorderColor = MaterialTheme.colors.primary, textColor = MaterialTheme.colors.onSecondary)

    )
}

//fun updateEmail(mailtext: String) {
//    userGuess = mailtext
//}

@Composable
fun TextFieldWithPassword() {
    var password by remember { mutableStateOf(TextFieldValue("")) }
    var passwordVisible by remember { mutableStateOf(false) }
    val focusManager = LocalFocusManager.current

    return OutlinedTextField(
        value = password,
        onValueChange = { password = it },
        label = { Text("Password",style = Typography.body2,color = MaterialTheme.colors.secondary) },
       // singleLine = true,
        placeholder = { Text("Enter password",style = Typography.body1,color = MaterialTheme.colors.primary) },
        visualTransformation = if (passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
        keyboardOptions = KeyboardOptions.Default.copy( imeAction = ImeAction.Done,keyboardType = KeyboardType.Password),
        keyboardActions = KeyboardActions(
            onDone = {focusManager.clearFocus()}
        ),
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = MaterialTheme.colors.secondary,
            unfocusedBorderColor = MaterialTheme.colors.primary, textColor = MaterialTheme.colors.onSecondary),
        trailingIcon = {
            val image = if (passwordVisible)
                Icons.Filled.Visibility
            else Icons.Filled.VisibilityOff

            // Please provide localized description for accessibility services
            val description = if (passwordVisible) "Hide password" else "Show password"

            IconButton(onClick = {passwordVisible = !passwordVisible}){
                Icon(imageVector  = image, description, tint = MaterialTheme.colors.secondary,)
            }
        }
    )
}

@Composable
fun ButtonOne(
) {
    val mContext= LocalContext.current
    Button(colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.primaryVariant),onClick = {
        val intent= Intent(mContext, GatitosActivityInicial::class.java)
        mContext.startActivity(intent)
    }) {
        Text(
            text = ("LOG IN"),
            style = Typography.body2,
            color = MaterialTheme.colors.secondary,
            modifier = Modifier
                .padding(8.dp)
        )
    }

}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    Proyecto3GatitosMiauMiauMiauTheme {
     //   ShowLogIn("usuario@degatitos.com")
    }
}