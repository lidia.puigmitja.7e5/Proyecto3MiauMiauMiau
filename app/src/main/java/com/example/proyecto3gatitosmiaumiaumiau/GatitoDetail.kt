package com.example.proyecto3gatitosmiaumiaumiau

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.proyecto3gatitosmiaumiaumiau.model.GatitosPhoto
import com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Typography
import com.example.proyecto3gatitosmiaumiaumiau.ui.ui.theme.Proyecto3GatitosMiauMiauMiauTheme
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class GatitoDetail : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent=intent
        val gatitoInfoStr=intent.getStringExtra("GatitoInfo")
        val gatitoInfo=gatitoInfoStr?.let { Json.decodeFromString<GatitosPhoto>(it) }
        setContent {
            Proyecto3GatitosMiauMiauMiauTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    //color = MaterialTheme.colors.background
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(color = MaterialTheme.colors.background)
                            .padding(5.dp)
                            .verticalScroll(rememberScrollState()),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally,

                        ) {
                        if (gatitoInfo != null) {
                            FullCatCard(gatitoInfo)
                        }
                    }
                }

            }            }
        }
    }


@Composable
fun FullCatCard(gatitoInfo: GatitosPhoto) {
    Card(
        border = BorderStroke(
            4.dp,
            color = MaterialTheme.colors.primary
        ),
        elevation = 5.dp,
        backgroundColor = MaterialTheme.colors.background) {
        Column() {
            GlideImage(
                imageModel = { gatitoInfo.image!!.url }, modifier = Modifier
                    .height(400.dp)
                    .fillMaxWidth(),
                imageOptions = ImageOptions(contentScale = ContentScale.Crop)
            )
            Description(gatitoInfo)
        }

    }


}

@Composable
fun Description(gatitoInfo: GatitosPhoto) {
    Column(modifier = Modifier.padding(4.dp)) {
        Spacer(modifier = Modifier.height(10.dp))
        Text(
            text = "Description: ",
            color = MaterialTheme.colors.primary,
            style = Typography.body2,
            modifier = Modifier
                .padding(8.dp)
        )
        Divider(color = MaterialTheme.colors.primary, thickness = 1.dp, modifier = Modifier.padding(start = 8.dp, end = 20.dp))
        Spacer(modifier = Modifier.height(10.dp))
        gatitoInfo.description?.let {
            Text(
                text = it,
                color = MaterialTheme.colors.secondary,
                modifier = Modifier
                    .padding(start = 15.dp, end = 15.dp),
                textAlign = TextAlign.Justify
            )
        }
        Spacer(modifier = Modifier.height(10.dp))

        Text(
            text = "Breeds: ",
            color = MaterialTheme.colors.primary,
            style = Typography.body2,
            modifier = Modifier
                .padding(8.dp)
        )
        Divider(color = MaterialTheme.colors.primary, thickness = 1.dp, modifier = Modifier.padding(start = 8.dp, end = 20.dp))

        Spacer(modifier = Modifier.height(10.dp))
        gatitoInfo.name?.let {
            Text(
                text = it,
                color = MaterialTheme.colors.secondary,
                modifier = Modifier
                    .padding(start = 15.dp, end = 15.dp),
                textAlign = TextAlign.Justify
            )
        }
        Spacer(modifier = Modifier.height(10.dp))
        Text(
            text = "Temperament: ",
            color = MaterialTheme.colors.primary,
            style = Typography.body2,
            modifier = Modifier
                .padding(8.dp)
        )
        Divider(color = MaterialTheme.colors.primary, thickness = 1.dp, modifier = Modifier.padding(start = 8.dp, end = 20.dp))

        Spacer(modifier = Modifier.height(10.dp))
        gatitoInfo.temperament?.let {
            Text(
                text = it,
                color = MaterialTheme.colors.secondary,
                modifier = Modifier
                    .padding(start = 15.dp, end = 15.dp),
                textAlign = TextAlign.Justify
            )
        }

        Spacer(modifier = Modifier.height(10.dp))
        Text(
            text = "Wiki: ",
            color = MaterialTheme.colors.primary,
            style = Typography.body2,
            modifier = Modifier
                .padding(8.dp)
        )
        Divider(color = MaterialTheme.colors.primary, thickness = 1.dp, modifier = Modifier.padding(start = 8.dp, end = 20.dp))

        Spacer(modifier = Modifier.height(10.dp))
        gatitoInfo.wikipediaUrl?.let {
            ButtonWiki(Url = gatitoInfo.wikipediaUrl!!)
        }
        Spacer(modifier = Modifier.height(10.dp))
    }

}

@Composable
fun ButtonWiki(Url:String){
    val context= LocalContext.current
    val intent= remember {
        Intent(Intent.ACTION_VIEW, Uri.parse(Url))
    }
    Box(modifier = Modifier.clickable { context.startActivity(intent) }){
        Text(
            text = Url,
            color = MaterialTheme.colors.secondary,
            modifier = Modifier
                .padding(start = 15.dp, end = 15.dp),
            textAlign = TextAlign.Justify
        )
    }
}


@Preview(showBackground = true)
@Composable
fun DefaultPreview2() {
    Proyecto3GatitosMiauMiauMiauTheme {
       // Greeting2("Android")
    }
}