package com.example.proyecto3gatitosmiaumiaumiau.ui.screens

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.proyecto3gatitosmiaumiaumiau.model.GatitosPhoto
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

sealed interface GatitosUiState{
    data class Success(val photos:List<GatitosPhoto>, val SelectCountry: String): GatitosUiState
    object Error: GatitosUiState
    object Loading: GatitosUiState
}


class GatitosViewModel: ViewModel() {
    var gatitosUiState: GatitosUiState by mutableStateOf(GatitosUiState.Loading)
        private set

    init{
        getGatitosPhotos()
    }

     fun getGatitosPhotos(){
        viewModelScope.launch {
            gatitosUiState=try {
                val listResult = GatitosApi.retrofitService.getPhotos()
                GatitosUiState.Success(listResult, "SelectCountry")
            } catch (e: IOException){
                GatitosUiState.Error
            } catch (e: HttpException){
                GatitosUiState.Error
            }
        }
    }
    
    fun SetSelectCountry(newCountry: String){
        if(gatitosUiState is GatitosUiState.Success){
            gatitosUiState=(gatitosUiState as GatitosUiState.Success).copy(SelectCountry = newCountry)
        }
    }
}