package com.example.proyecto3gatitosmiaumiaumiau.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.proyecto3gatitosmiaumiaumiau.R
import com.example.proyecto3gatitosmiaumiaumiau.ui.screens.GatitosViewModel
import com.example.proyecto3gatitosmiaumiaumiau.ui.screens.HomeScreen
import com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Proyecto3GatitosMiauMiauMiauTheme

@Composable
fun GatitosPhotosApp(modifier: Modifier = Modifier) {
    Scaffold(
        modifier = modifier.fillMaxSize().background(color = MaterialTheme.colors.onBackground),
        topBar = { TopAppBar(title= {Text(stringResource(R.string.app_name), color = MaterialTheme.colors.background) })}
    ) {
        Proyecto3GatitosMiauMiauMiauTheme {
            Surface(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(it),
                color = MaterialTheme.colors.onBackground
            ) {
                val gatitosViewModel: GatitosViewModel = viewModel()
                HomeScreen(gatitosUiState = gatitosViewModel.gatitosUiState,
                    setSelectCountry = { gatitosViewModel.SetSelectCountry(it) })
            }
        }
}
}
