package com.example.proyecto3gatitosmiaumiaumiau.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Purple800 = Color(0xFF2740C9)
val Teal200 = Color(0xFF03DAC5)
val FondoDark=Color(0xFF3C3C3C)
val CardFondoDark=Color(0xFF999999)