package com.example.proyecto3gatitosmiaumiaumiau.ui.screens

import android.content.Intent
import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.LineHeightStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.proyecto3gatitosmiaumiaumiau.GatitoDetail
import com.example.proyecto3gatitosmiaumiaumiau.R
import com.example.proyecto3gatitosmiaumiaumiau.model.GatitosPhoto
import com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Proyecto3GatitosMiauMiauMiauTheme
import com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Typography
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

@Composable
fun HomeScreen(
    gatitosUiState: GatitosUiState,
    modifier: Modifier = Modifier,
    setSelectCountry: (String) -> Unit
) {
    when (gatitosUiState) {
        is GatitosUiState.Loading -> LoadingScreen(modifier)
        is GatitosUiState.Success -> ResultScreen(
            gatitosUiState.photos,
            gatitosUiState.SelectCountry,
            setSelectCountry,
            modifier)
        is GatitosUiState.Error -> ErrorScreen(modifier)
    }
}

@Composable
fun LoadingScreen(modifier: Modifier = Modifier) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colors.background)
    ) {
       /* GlideImage(
            modifier = Modifier.size(200.dp),
            painter = painterResource(id = R.drawable.loading_gatitos),
            contentDescription = stringResource(id = R.string.loading)
        )*/
        GlideImage(
            imageModel = { (R.drawable.loading_gatitos) }, modifier = Modifier
                .height(400.dp)
                .fillMaxWidth(),
            imageOptions = ImageOptions(contentScale = ContentScale.Crop)
        )
    }
}

@Composable
fun ErrorScreen(modifier: Modifier = Modifier) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colors.background)
    ) {
        Column(Modifier.padding(8.dp)) {
            GlideImage(
                imageModel = { (R.drawable.errorcat) }, modifier = Modifier
                    .height(400.dp)
                    .fillMaxWidth(),
                imageOptions = ImageOptions(contentScale = ContentScale.Crop)
            )
            Spacer(modifier = Modifier.height(10.dp))
            Text(stringResource(id = R.string.loading_failed), color = MaterialTheme.colors.secondary, modifier=Modifier.padding(start = 35.dp), style = MaterialTheme.typography.body2)
        }

    }
}

@Composable
fun ResultScreen(gatitosInfo: List<GatitosPhoto>, selectCountry: String, setSelectCountry: (String) -> Unit, modifier: Modifier = Modifier) {
    Column(modifier) {
        var gatitosInfoListaFiltrada= gatitosInfo
        SeleccionCountry(selectCountry, setSelectCountry)
        if(selectCountry!="SelectCountry"){
            gatitosInfoListaFiltrada=gatitosInfo.filter { it.countryCode==selectCountry }

        }
        Spacer(modifier=Modifier.height(2.dp))
        GatitosList(gatitosInfoListaFiltrada, Modifier.background(color = MaterialTheme.colors.onBackground))

    }
}

@Composable
fun SeleccionCountry(selectCountry: String, setSelectCountry: (String) -> Unit) {
    val listItems =
        arrayOf(
            "SelectCountry",
            "EG",
            "US",
            "CA",
            "ES",
            "BR",
            "GR",
            "AE",
            "AU",
            "FR",
            "GB",
            "MM",
            "CY",
            "RU",
            "CN",
            "JP",
            "TH",
            "IM",
            "NO",
            "IR",
            "SP",
            "SO",
            "TR"
        )
    val contextForToast = LocalContext.current.applicationContext

    // state of the menu
    var expanded by remember {
        mutableStateOf(false)
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = MaterialTheme.colors.background)
            .padding(5.dp),
        verticalAlignment = Alignment.Top,
        horizontalArrangement = Arrangement.Start,

    ) {

        ButtonOne(
            expanded = expanded,
            onClick = { expanded = !expanded },
            selectCountry=selectCountry,
        )

    // drop down menu
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = {
                expanded = false
            },
            modifier = Modifier
                .height(250.dp)
                .background(color = MaterialTheme.colors.primaryVariant)
        ) {
            // adding items
            listItems.forEach { itemValue ->
                DropdownMenuItem(
                    onClick = {
                        setSelectCountry(itemValue)
                        Toast.makeText(contextForToast, "Country: $itemValue", Toast.LENGTH_SHORT)
                            .show()
                        expanded = false
                    },
                    enabled = (itemValue != selectCountry)
                ) {
                    Text(text = itemValue,style = Typography.body2,
                        color = MaterialTheme.colors.secondary)
                }
            }
        }

}
}

@Composable
fun ButtonOne(
    expanded: Boolean,
    onClick: () -> Unit,
    selectCountry: String,
    modifier: Modifier = Modifier

) {

        Button(onClick = onClick,colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.primaryVariant)) {

            Text(text = selectCountry,style = Typography.button,
            color = MaterialTheme.colors.secondary,
            textAlign = TextAlign.Center)

            Icon(
                imageVector = if (expanded) Icons.Filled.ExpandLess else Icons.Filled.ExpandMore,
                tint = MaterialTheme.colors.secondary,
                contentDescription = "IconButton"
            )

    }




}


@Composable
fun GatitosList(gatitosList: List<GatitosPhoto>, modifier: Modifier = Modifier) {
    val mContext= LocalContext.current
    LazyColumn(modifier = modifier) {
        items(items = gatitosList) { gatitoPhoto ->
            Button(onClick = { val intent=Intent(mContext, GatitoDetail::class.java)
                val gatitoInfoStr= Json.encodeToString(gatitoPhoto)
                intent.putExtra("GatitoInfo", gatitoInfoStr)
                mContext.startActivity(intent) },colors = ButtonDefaults.buttonColors(backgroundColor = MaterialTheme.colors.background)) {

            GatitosCard(
                gatitoPhoto = gatitoPhoto,
                modifier = Modifier.background(color = MaterialTheme.colors.background)
            )
            }
        }
    }
}


@Composable
fun GatitosCard(gatitoPhoto: GatitosPhoto, modifier: Modifier = Modifier) {
    // TODO 1. Your card UI
    /*  var expanded by remember { mutableStateOf(false) }
      val color by animateColorAsState(
          targetValue = if (expanded) MaterialTheme.colors.primaryVariant else MaterialTheme.colors.background,
      )*/
    Card(
        modifier = modifier,
        border = BorderStroke(
            4.dp,
            color = MaterialTheme.colors.primary
        ),
        elevation = 5.dp,
       // backgroundColor = MaterialTheme.colors.background
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp)
                .background(color = MaterialTheme.colors.background),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,

            ) {
            GlideImage(
                imageModel = { gatitoPhoto.image!!.url }, modifier = Modifier
                    .size(200.dp),
                imageOptions = ImageOptions(contentScale = ContentScale.Crop)
            )
            Spacer(modifier=Modifier.width(8.dp))
            Column( modifier = Modifier
                .background(color=MaterialTheme.colors.background),) {
                gatitoPhoto.name?.let{
                    Text(
                        style = Typography.body2,
                        text = it,
                        color = MaterialTheme.colors.secondary
                    )
                }

                gatitoPhoto.description?.let {
                    Text(
                        style = Typography.body1,
                        text = it,
                        color = MaterialTheme.colors.primary,
                        overflow = TextOverflow.Ellipsis,
                        maxLines = 2
                    )
                }

            }

        }
    }
}







@Preview(showBackground = true)
@Composable
fun LoadingScreenPreview() {
    Proyecto3GatitosMiauMiauMiauTheme {
        LoadingScreen()
    }
}

@Preview(showBackground = true)
@Composable
fun ErrorScreenPreview() {
    Proyecto3GatitosMiauMiauMiauTheme {
        ErrorScreen()
    }
}

@Preview(showBackground = true)
@Composable
fun ResultScreenPreview() {
    Proyecto3GatitosMiauMiauMiauTheme {
     //   ResultScreen(stringResource(R.string.result))
    }
}

@Preview
@Composable
private fun GatitosCardPreview() {
    // TODO 2. Preview your card
    /*  CatCard(
          catPhoto =  PreviewData().loadCats().first()
      )*/

}
