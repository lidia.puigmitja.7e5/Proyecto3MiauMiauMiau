package com.example.proyecto3gatitosmiaumiaumiau.ui.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private val DarkColorPalette = darkColors(
    primary = com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Purple200,
    primaryVariant = com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Purple700,
    secondary = com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Teal200,
    background = com.example.proyecto3gatitosmiaumiaumiau.ui.theme.FondoDark,
    surface = com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Purple200,
    onPrimary = Color.Black,
    onSecondary = Color.White,
    onBackground = com.example.proyecto3gatitosmiaumiaumiau.ui.theme.CardFondoDark,
    onSurface = Color.Black,
)

private val LightColorPalette = lightColors(
    primary = com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Purple500,
    primaryVariant = com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Teal200,
    secondary = com.example.proyecto3gatitosmiaumiaumiau.ui.theme.Purple800,
    background = Color.White,
    surface = Color.White,
    onPrimary = Color.White,
    onSecondary = Color.Black,
    onBackground = Color.White,
    onSurface = Color.White,

    )

@Composable
fun Proyecto3GatitosMiauMiauMiauTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}