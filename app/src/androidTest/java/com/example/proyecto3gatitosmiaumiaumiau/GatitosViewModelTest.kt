package com.example.proyecto3gatitosmiaumiaumiau

import androidx.compose.ui.test.junit4.createAndroidComposeRule
import com.example.proyecto3gatitosmiaumiaumiau.ui.screens.GatitosUiState
import com.example.proyecto3gatitosmiaumiaumiau.ui.screens.GatitosViewModel
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test

class GatitosViewModelTest {

    private val viewModel= GatitosViewModel()
    @get: Rule
    val composeTestRule= createAndroidComposeRule<GatitosActivityInicial>()


    @Test
    fun testViewModelSuccess(){
        viewModel.getGatitosPhotos()
        composeTestRule.waitUntil(3000) {
            viewModel.gatitosUiState!= GatitosUiState.Loading
        }

        assertTrue(viewModel.gatitosUiState is GatitosUiState.Success)

    }

}